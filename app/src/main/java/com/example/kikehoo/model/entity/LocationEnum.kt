package com.example.kikehoo.model.entity

enum class LocationEnum {
    UNKNOWN,
    WORK,
    REMOTE,
    VACATION
}