package com.example.kikehoo.model.repository

import androidx.lifecycle.LiveData
import com.example.kikehoo.model.dao.PersonDao
import com.example.kikehoo.model.entity.Person

class PersonRepository(private val dao: PersonDao) {
    val personList: LiveData<List<Person>> = dao.getPersonList()
    val nbPersons: LiveData<Int> = dao.getNbPersons()
    val profile: LiveData<Person> = dao.getProfile()

    suspend fun insert(person: Person) {
        dao.insert(person)
    }
    suspend fun update(person: Person) {
        dao.update(person)
    }
    suspend fun delete(person: Person) {
        dao.delete(person)
    }
    suspend fun deleteAll() {
        dao.deleteAll()
    }
}