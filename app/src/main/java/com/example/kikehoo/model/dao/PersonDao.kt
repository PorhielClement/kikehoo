package com.example.kikehoo.model.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.kikehoo.model.entity.Person
@Dao
interface PersonDao {
    @Query("SELECT * FROM table_person WHERE id = 1")
    fun getProfile(): LiveData<Person>

    @Query("SELECT * FROM table_person WHERE id > 1 ORDER BY name ASC")
    fun getPersonList(): LiveData<List<Person>>

    @Query("SELECT COUNT() FROM table_person WHERE ID > 1")
    fun getNbPersons(): LiveData<Int>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(person: Person)

    @Update
    fun update(person: Person)

    @Delete
    fun delete(person: Person)

    @Query("DELETE FROM table_person")
    fun deleteAll()
}
