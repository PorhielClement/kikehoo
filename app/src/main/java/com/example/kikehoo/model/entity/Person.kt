package com.example.kikehoo.model.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
@Entity(tableName = "table_person")
data class Person(
    @PrimaryKey(autoGenerate = true) var id: Int,
    val name: String,
    val image: String,
    val contacts: List<Contact>,
    val week: Int,
    val locations: List<Location>
) : Parcelable {
    fun toJson(): String {
        val moshi = Moshi.Builder().build()
        val adapter: JsonAdapter<Person> = moshi.adapter(Person::class.java)
        return adapter.toJson(this)
    }
    companion object {
        fun fromJson(json: String): Person? {
            val moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<Person> = moshi.adapter(Person::class.java)
            return adapter.fromJson(json)
        }
    }
}