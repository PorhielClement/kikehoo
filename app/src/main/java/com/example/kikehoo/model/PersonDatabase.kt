package com.example.kikehoo.model

import android.content.Context
import androidx.room.*
import com.example.kikehoo.model.converter.Converters
import com.example.kikehoo.model.dao.PersonDao
import com.example.kikehoo.model.entity.Person
import kotlinx.coroutines.InternalCoroutinesApi

@Database(entities = [Person::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class PersonDatabase : RoomDatabase() {

    abstract fun personDao(): PersonDao

    companion object {
        @Volatile
        private var DATABASE_INSTANCE: PersonDatabase? = null

        @InternalCoroutinesApi
        fun getDatabase(context: Context): PersonDatabase {
            val tmpInstance = DATABASE_INSTANCE
            if (tmpInstance != null) return tmpInstance

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PersonDatabase::class.java,
                    "person_database"
                ).build()
                DATABASE_INSTANCE = instance
                return instance
            }
        }
    }
}