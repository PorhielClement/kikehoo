package com.example.kikehoo.model.converter

import androidx.room.TypeConverter
import com.example.kikehoo.model.entity.Contact
import com.example.kikehoo.model.entity.Location
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class Converters {
    private val gson = Gson()

    @TypeConverter
    fun ContactListFromJson(data: String?): List<Contact?>? {
        if (data == null) return null
        val type: Type = object : TypeToken<List<Contact?>?>() {}.type
        return gson.fromJson<List<Contact>>(data, type)
    }

    @TypeConverter
    fun LocationListFromJson(data: String?): List<Location?>? {
        if (data == null) return null
        val type: Type = object : TypeToken<List<Location?>?>() {}.type
        return gson.fromJson<List<Location>>(data, type)
    }

    @TypeConverter
    fun contactFromJson(data: String): Contact? {
        return Contact.fromJson(data)
    }

    @TypeConverter
    fun locationFromJson(data: String): Location? {
        return Location.fromJson(data)
    }

    @TypeConverter
    fun contactListToJson(data: List<Contact?>?): String? {
        if (data == null) return null
        val type: Type = object:TypeToken<List<Contact?>?>() {}.type
        return gson.toJson(data, type)
    }

    @TypeConverter
    fun locationListToJson(data: List<Location?>?): String? {
        if (data == null) return null
        val type: Type = object:TypeToken<List<Location?>?>() {}.type
        return gson.toJson(data, type)
    }

    @TypeConverter
    fun contactToJson(data: Contact?): String? {
        return data?.toJson()
    }

    @TypeConverter
    fun locationToJson(data: Location?): String? {
        return data?.toJson()
    }
}