package com.example.kikehoo.model.entity

import android.icu.util.Calendar
import android.os.Parcelable
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import kotlinx.parcelize.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
class Location(var day: Int, var value: String): Parcelable {
    constructor():this(Calendar.MONDAY, LocationEnum.WORK.name)

    fun toJson(): String {
        val moshi = Moshi.Builder().build()
        val adapter: JsonAdapter<Location> = moshi.adapter(Location::class.java)
        return adapter.toJson(this)
    }

    companion object {
        fun fromJson(json: String): Location? {
            val moshi = Moshi.Builder().build()
            val adapter: JsonAdapter<Location> = moshi.adapter(Location::class.java)
            return adapter.fromJson(json)
        }
    }
}