package com.example.kikehoo.view.profile

import android.app.Application
import com.example.kikehoo.view.MasterViewModel
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
class ProfileUpdateViewModel(application: Application): MasterViewModel(application) {
}