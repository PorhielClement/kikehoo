package com.example.kikehoo.view.list

import android.icu.util.Calendar
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import androidx.annotation.RequiresApi
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView

import com.squareup.picasso.Picasso

import com.example.kikehoo.R

import com.example.kikehoo.model.entity.Location
import com.example.kikehoo.model.entity.LocationEnum
import com.example.kikehoo.model.entity.Person

class ListAdapter : RecyclerView.Adapter<ListAdapter.ViewHolder>() {

    private var personList: List<Person> = emptyList()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val personListItemUserPicture: ImageView =
            view.findViewById(R.id.person_list_item_user_picture)
        private val personListItemUserName: TextView =
            view.findViewById(R.id.person_list_item_user_name)
        private val personListItemTodaysAvailability: ImageView =
            view.findViewById(R.id.person_list_item_todays_availability)

        fun bind(data: Person) {
            personListItemUserName.text = data.name
            if(data.image.isNotEmpty()) {
                Picasso.get().load(data.image).into(personListItemUserPicture)
            }
            personListItemTodaysAvailability.setImageResource(getAvailability(data.locations))
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                return ViewHolder(layoutInflater.inflate(R.layout.person_list_item_layout, parent, false))
            }
        }
        @RequiresApi(Build.VERSION_CODES.N)
        fun getAvailability(locations: List<Location>): Int{
            val calendar: Calendar = Calendar.getInstance()
            return getSource(locations.find{it.day==calendar.get(Calendar.DAY_OF_WEEK)})
        }

        private fun getSource(location: Location?) : Int {
            location ?: return R.drawable.ic_off
            val result: Int = when(location.value) {
                LocationEnum.WORK.name -> R.drawable.ic_workcase
                LocationEnum.REMOTE.name -> R.drawable.ic_home
                LocationEnum.VACATION.name -> R.drawable.ic_sun
                else -> R.drawable.ic_off
            }
            return result
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(LayoutInflater.from(viewGroup.context).inflate(R.layout.person_list_item_layout, viewGroup, false) as ViewGroup)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = personList[position]
        viewHolder.bind(item)
        viewHolder.itemView.setOnClickListener {
            val action = FragmentListDirections.actionFragmentListToFragmentPerson(item)
            viewHolder.itemView.findNavController().navigate(action)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = personList.size

    fun setData(personList: List<Person>) {
        this.personList = personList
        notifyDataSetChanged()
    }
}
