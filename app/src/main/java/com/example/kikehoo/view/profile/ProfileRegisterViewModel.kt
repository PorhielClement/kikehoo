package com.example.kikehoo.view.profile

import android.app.Application
import com.example.kikehoo.view.MasterViewModel
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
class ProfileRegisterViewModel(application: Application) : MasterViewModel(application)