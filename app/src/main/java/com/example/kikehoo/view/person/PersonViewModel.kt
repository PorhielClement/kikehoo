package com.example.kikehoo.view.person

import android.app.Application
import com.example.kikehoo.view.MasterViewModel
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
class PersonViewModel(application: Application) : MasterViewModel(application)