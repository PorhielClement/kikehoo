package com.example.kikehoo.view.qr

import android.icu.util.Calendar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.kikehoo.R
import com.example.kikehoo.databinding.FragmentNewPersonBinding
import com.example.kikehoo.model.entity.Location
import com.example.kikehoo.model.entity.LocationEnum
import com.squareup.picasso.Picasso
import kotlinx.coroutines.InternalCoroutinesApi

class FragmentNewPerson : Fragment() {
    private lateinit var binding: FragmentNewPersonBinding

    @InternalCoroutinesApi
    private lateinit var newPersonViewModel: NewPersonViewModel

    private val args: FragmentNewPersonArgs by navArgs()

    @OptIn(InternalCoroutinesApi::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewPersonBinding.inflate(inflater, container, false)
        newPersonViewModel = ViewModelProvider(this)[NewPersonViewModel::class.java]
        val person = args.newPerson

        binding.newPersonUserName.text = person.name
        binding.newPersonUserEmail.text = person.contacts.find { it.key == "Email" }?.value
        binding.newPersonUserPhone.text = person.contacts.find { it.key == "Phone" }?.value
        binding.newPersonUserFb.text = person.contacts.find { it.key == "Facebook" }?.value
        if(person.image.isNotEmpty())
            Picasso.get().load(person.image).into(binding.newPersonUserPicture)
        person.locations.forEach { setLocation(it) }
        binding.newPersonActionDelete.setOnClickListener {
            if (newPersonViewModel.nbPerson.value == 0) {
                findNavController().navigate(R.id.action_fragmentNewPerson_to_fragmentRegister)
            } else {
                findNavController().navigate(R.id.action_fragmentNewPerson_to_fragmentList)
            }
        }
        binding.newPersonActionSave.setOnClickListener {
            if (person.id == 0) {
                newPersonViewModel.addPerson(person)
            } else {
                newPersonViewModel.updatePerson(person)
            }
            Thread.sleep(2_000)
            findNavController().navigate(R.id.action_fragmentNewPerson_to_fragmentList)
        }

        return binding.root
    }

    private fun setLocation(location: Location) = when (location.day) {
        Calendar.MONDAY -> binding.newPersonLocationMonIc
        Calendar.TUESDAY -> binding.newPersonLocationTueIc
        Calendar.WEDNESDAY -> binding.newPersonLocationWedIc
        Calendar.THURSDAY -> binding.newPersonLocationThuIc
        else -> binding.newPersonLocationFriIc
    }.let {
        when (location.value) {
            LocationEnum.WORK.name -> {
                it.setImageResource(R.drawable.ic_workcase)
                it.contentDescription = getString(R.string.ic_desc_disp_work)
            }
            LocationEnum.REMOTE.name -> {
                it.setImageResource(R.drawable.ic_home)
                it.contentDescription = getString(R.string.ic_desc_disp_home)
            }
            LocationEnum.VACATION.name -> {
                it.setImageResource(R.drawable.ic_sun)
                it.contentDescription = getString(R.string.ic_desc_disp_holiday)
            }
            else -> {
                it.setImageResource(R.drawable.ic_off)
                it.contentDescription = getString(R.string.ic_desc_disp_off)
            }
        }
    }
}