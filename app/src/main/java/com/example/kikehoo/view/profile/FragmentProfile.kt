package com.example.kikehoo.view.profile

import android.graphics.Bitmap
import android.graphics.Color
import android.icu.util.Calendar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.kikehoo.R
import com.example.kikehoo.databinding.FragmentProfileBinding
import com.example.kikehoo.model.entity.Location
import com.example.kikehoo.model.entity.LocationEnum
import com.example.kikehoo.model.entity.Person
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.picasso.Picasso
import kotlinx.coroutines.InternalCoroutinesApi

class FragmentProfile : Fragment() {
    @InternalCoroutinesApi
    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var binding: FragmentProfileBinding

    private val noCompression = 100
    private val qrCodeFileName = "qrcode"

    @OptIn(InternalCoroutinesApi::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

            binding = FragmentProfileBinding.inflate(inflater, container, false)
        profileViewModel = ViewModelProvider(this)[ProfileViewModel::class.java]
        profileViewModel.profile.observe(viewLifecycleOwner, { person ->
            binding.profileDetailCardUserName.text = person.name
            if(person.image.isNotEmpty())
                Picasso.get().load(person.image).into(binding.profileDetailCardUserPicture)
            binding.profileDetailInfoUserFb.text =
                person.contacts.find { it.key == "Facebook" }?.value
            binding.profileDetailInfoUserPhone.text =
                person.contacts.find { it.key == "Phone" }?.value
            binding.profileDetailInfoUserEmail.text =
                person.contacts.find { it.key == "Email" }?.value
            person.locations.forEach { setLocation(it) }
            binding.profileDetailCardActionPop.setOnClickListener {
                findNavController().navigate(R.id.action_fragmentProfile_to_fragmentList)
            }
            binding.profileDetailCardActionEdit.setOnClickListener {
                val action = person?.let {
                    FragmentProfileDirections.actionFragmentProfileToFragmentUpdate(it)
                }
                action?.let { findNavController().navigate(it) }
            }
            binding.profileDetailCardActionQr.setOnClickListener {
                // findNavController().navigate(R.id.action_fragmentProfile_to_fragmentQRGen)
                val qrCodeWriter = QRCodeWriter()

                val bitMatrix = qrCodeWriter.encode(person.toString(), BarcodeFormat.QR_CODE, 512, 512)
                val width = bitMatrix.width
                val height = bitMatrix.height
                val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
                for (x in 0 until width)
                    for (y in 0 until height)
                        bitmap.setPixel(x,y, if(bitMatrix.get(x,y)) Color.BLACK else Color.WHITE)
                binding.profileDetailInfoQrcode.setImageBitmap(bitmap)
            }
        })
        return binding.root
    }

    private fun setLocation(location: Location) = when (location.day) {
        Calendar.MONDAY -> binding.profileDetailInfoLocationMonIc
        Calendar.TUESDAY -> binding.profileDetailInfoLocationTueIc
        Calendar.WEDNESDAY -> binding.profileDetailInfoLocationWedIc
        Calendar.THURSDAY -> binding.profileDetailInfoLocationThuIc
        else -> binding.profileDetailInfoLocationFriIc
    }.let {
        when (location.value) {
            LocationEnum.WORK.name -> {
                it.setImageResource(R.drawable.ic_workcase)
                it.contentDescription = getString(R.string.ic_desc_disp_work)
            }
            LocationEnum.REMOTE.name -> {
                it.setImageResource(R.drawable.ic_home)
                it.contentDescription = getString(R.string.ic_desc_disp_home)
            }
            LocationEnum.VACATION.name -> {
                it.setImageResource(R.drawable.ic_sun)
                it.contentDescription = getString(R.string.ic_desc_disp_holiday)
            }
            else -> {
                it.setImageResource(R.drawable.ic_off)
                it.contentDescription = getString(R.string.ic_desc_disp_off)
            }
        }
    }
}