package com.example.kikehoo.view.profile

import android.icu.util.Calendar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.kikehoo.R
import com.example.kikehoo.databinding.FragmentRegisterBinding
import com.example.kikehoo.model.entity.Contact
import com.example.kikehoo.model.entity.Location
import com.example.kikehoo.model.entity.LocationEnum
import com.example.kikehoo.model.entity.Person
import kotlinx.coroutines.InternalCoroutinesApi

class FragmentProfileRegister : Fragment() {
    private lateinit var binding: FragmentRegisterBinding

    @InternalCoroutinesApi
    private lateinit var profileRegisterViewModel: ProfileRegisterViewModel

    @OptIn(InternalCoroutinesApi::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        profileRegisterViewModel = ViewModelProvider(this)[ProfileRegisterViewModel::class.java]
        profileRegisterViewModel.nbPerson.observe(viewLifecycleOwner,
            { nb ->
                if (nb > 0) {
                    findNavController().navigate(R.id.action_fragmentRegister_to_fragmentList)
                }
            }
        )
        binding.profileRegisterActionSave.setOnClickListener {
            val newName = binding.profileRegisterUserName.text.toString()
            val newImage = binding.profileRegisterUserPicture.text.toString()
            val newContactList = listOf(
                Contact("Facebook", binding.profileRegisterUserFb.text.toString()),
                Contact("Phone", binding.profileRegisterUserPhone.text.toString()),
                Contact("Email", binding.profileRegisterUserEmail.text.toString())
            )
            val newWeek = Calendar.WEEK_OF_YEAR
            val newLocationList = listOf(
                Location(Calendar.MONDAY, LocationEnum.UNKNOWN.name),
                Location(Calendar.TUESDAY, LocationEnum.UNKNOWN.name),
                Location(Calendar.WEDNESDAY, LocationEnum.UNKNOWN.name),
                Location(Calendar.THURSDAY, LocationEnum.UNKNOWN.name),
                Location(Calendar.FRIDAY, LocationEnum.UNKNOWN.name),
            )
            val newProfile = Person(0, newName, newImage, newContactList, newWeek, newLocationList)
            profileRegisterViewModel.signUp(newProfile)
            Thread.sleep(2_000)
            findNavController().navigate(R.id.action_fragmentRegister_to_fragmentList)
        }

        return binding.root
    }
}