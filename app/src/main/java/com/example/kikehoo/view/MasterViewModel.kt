package com.example.kikehoo.view

import android.app.Application
import androidx.lifecycle.*
import com.example.kikehoo.model.PersonDatabase
import com.example.kikehoo.model.dao.PersonDao
import com.example.kikehoo.model.entity.Person
import com.example.kikehoo.model.repository.PersonRepository
import com.squareup.moshi.*
import kotlinx.coroutines.*

@InternalCoroutinesApi
open class MasterViewModel(application: Application): AndroidViewModel(application) {
    private val repository: PersonRepository
    val personList: LiveData<List<Person>>
    val profile: LiveData<Person>
    val nbPerson: LiveData<Int>
    val dao: PersonDao =
        PersonDatabase.getDatabase(application).personDao()

    init {
        repository = PersonRepository(dao)
        profile = repository.profile
        personList = repository.personList
        nbPerson = repository.nbPersons
    }

    fun signUp(data: Person) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAll()
            repository.insert(data)
        }
    }

    fun addPerson(data: Person) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insert(data)
        }
    }

    fun updateProfile(data: Person) {
        viewModelScope.launch(Dispatchers.IO) {
            data.id = 1
            repository.update(data)
        }
    }

    fun updatePerson(data: Person) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.update(data)
        }
    }

    fun deletePerson(data: Person) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.delete(data)
        }
    }

    fun deleteAllPersons() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAll()
        }
    }

    fun getJson(data: Person): String {
        val moshi = Moshi.Builder().build()
        val adapter: JsonAdapter<Person> = moshi.adapter(Person::class.java)
        return adapter.toJson(data)
    }
}