package com.example.kikehoo.view.list

import android.app.Application
import com.example.kikehoo.view.MasterViewModel
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
class ListViewModel(application: Application) : MasterViewModel(application)