package com.example.kikehoo.view.qr

import android.app.Application
import com.example.kikehoo.view.MasterViewModel
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
class QrGenViewModel(application: Application): MasterViewModel(application)