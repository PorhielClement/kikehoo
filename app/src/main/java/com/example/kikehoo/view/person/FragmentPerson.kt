package com.example.kikehoo.view.person

import android.content.Intent
import android.icu.util.Calendar
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.kikehoo.R
import com.example.kikehoo.databinding.FragmentPersonBinding
import com.example.kikehoo.model.entity.Location
import com.example.kikehoo.model.entity.LocationEnum
import com.squareup.picasso.Picasso
import kotlinx.coroutines.InternalCoroutinesApi

class FragmentPerson : Fragment() {
    private lateinit var binding: FragmentPersonBinding

    @InternalCoroutinesApi
    private lateinit var personViewModel: PersonViewModel
    private val args: FragmentPersonArgs by navArgs()

    @OptIn(InternalCoroutinesApi::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPersonBinding.inflate(inflater, container, false)
        val person = args.person

        binding.personCardUserName.text = person.name
        if(person.image.isNotEmpty())
            Picasso.get().load(person.image).into(binding.personCardUserPicture)
        binding.personInfoUserFb.text = person.contacts.find { it.key == "Facebook" }?.value
        binding.personInfoUserPhone.text = person.contacts.find { it.key == "Phone" }?.value
        binding.personInfoUserEmail.text = person.contacts.find { it.key == "Email" }?.value
        binding.personInfoLocationMonIc.setImageResource(getImageResource(person.locations.find { it.day == Calendar.MONDAY }))
        binding.personInfoLocationTueIc.setImageResource(getImageResource(person.locations.find { it.day == Calendar.TUESDAY }))
        binding.personInfoLocationWedIc.setImageResource(getImageResource(person.locations.find { it.day == Calendar.WEDNESDAY }))
        binding.personInfoLocationThuIc.setImageResource(getImageResource(person.locations.find { it.day == Calendar.THURSDAY }))
        binding.personInfoLocationFriIc.setImageResource(getImageResource(person.locations.find { it.day == Calendar.FRIDAY }))
        personViewModel = ViewModelProvider(this)[PersonViewModel::class.java]
        binding.personCardActionPop.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentPerson_to_fragmentList)
        }
        binding.personCardActionDelete.setOnClickListener {
            personViewModel.deletePerson(person)
            Thread.sleep(2_000)
            findNavController().navigate(R.id.action_fragmentPerson_to_fragmentList)
        }
        binding.personCardActionPhone.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:")
            intent.putExtra(
                Intent.EXTRA_PHONE_NUMBER,
                binding.personInfoUserPhone.text.toString()
            )
            startActivity(intent)
        }
        binding.personCardActionEmail.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto:")
            intent.putExtra(Intent.EXTRA_EMAIL, binding.personInfoUserEmail.text.toString())
            startActivity(intent)
        }
        binding.personCardActionFb.setOnClickListener {
            val intent = Intent(Intent.ACTION_WEB_SEARCH)
            intent.data =
                Uri.parse("https://www.facebook.com/" + binding.personInfoUserFb.text.toString())
            startActivity(intent)
        }
        return binding.root
    }

    private fun getImageResource(location: Location?): Int {
        location ?: return R.drawable.ic_off
        val result: Int = when (location.value) {
            LocationEnum.WORK.name -> R.drawable.ic_workcase
            LocationEnum.REMOTE.name -> R.drawable.ic_home
            LocationEnum.VACATION.name -> R.drawable.ic_sun
            else -> R.drawable.ic_off
        }
        return result
    }
}