package com.example.kikehoo.view.profile

import android.icu.util.Calendar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.kikehoo.R
import com.example.kikehoo.databinding.FragmentUpdateBinding
import com.example.kikehoo.model.entity.Contact
import com.example.kikehoo.model.entity.Location
import com.example.kikehoo.model.entity.LocationEnum
import com.example.kikehoo.model.entity.Person
import kotlinx.coroutines.InternalCoroutinesApi

class FragmentProfileUpdate : Fragment() {
    private val args: FragmentProfileUpdateArgs by navArgs()

    @InternalCoroutinesApi
    private lateinit var profileUpdateViewModel: ProfileUpdateViewModel
    private lateinit var binding: FragmentUpdateBinding

    @OptIn(InternalCoroutinesApi::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUpdateBinding.inflate(inflater, container, false)
        profileUpdateViewModel = ViewModelProvider(this)[ProfileUpdateViewModel::class.java]


        val profile = args.updatedProfile
        binding.profileUpdateUserName.hint = profile.name
        binding.profileUpdateUserFb.hint = profile.contacts.find { it.key == "Facebook" }?.value
        binding.profileUpdateUserPhone.hint = profile.contacts.find { it.key == "Phone" }?.value
        binding.profileUpdateUserEmail.hint = profile.contacts.find { it.key == "Email" }?.value
        profile.locations.forEach { setLocation(it) }

        binding.profileUpdateActionPop.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentUpdate_to_fragmentProfile)
        }
        binding.profileUpdateActionSave.setOnClickListener {
            val newId = profile.id
            val newName = binding.profileUpdateUserName.let {
                when { it.text.isNotEmpty() -> it.text.toString() ; it.hint.isNotEmpty() -> it.hint.toString() ; else -> "" } }
            val newPicture = binding.profileUpdateUserPicture.let {
                when { it.text.isNotEmpty() -> it.text.toString() ; it.hint.isNotEmpty() -> it.hint.toString() ; else -> "" } }
            val newFb = binding.profileUpdateUserFb.let {
                when { it.text.isNotEmpty() -> it.text.toString() ; it.hint.isNotEmpty() -> it.hint.toString() ; else -> "" } }
            val newPhone = binding.profileUpdateUserPhone.let {
                when { it.text.isNotEmpty() -> it.text.toString() ; it.hint.isNotEmpty() -> it.hint.toString() ; else -> "" } }
            val newEmail = binding.profileUpdateUserEmail.let {
                when { it.text.isNotEmpty() -> it.text.toString() ; it.hint.isNotEmpty() -> it.hint.toString() ; else -> "" } }
            val newContactList = listOf(
                Contact("Facebook", newFb),
                Contact("Phone", newPhone),
                Contact("Email", newEmail)
            )
            val newWeek = Calendar.WEEK_OF_YEAR
            val newLocations = profile.locations
            val newProfile = Person(
                id = newId,
                name = newName,
                image = newPicture,
                contacts = newContactList,
                week = newWeek,
                locations = newLocations
            )
            profileUpdateViewModel.updateProfile(newProfile)
            Thread.sleep(2_000)
            findNavController().navigate(R.id.action_fragmentUpdate_to_fragmentProfile)
        }

        binding.profileUpdateLocationMonIc.setOnClickListener {
            binding.profileUpdateLocationMonIc.let { imageView ->
                val day: Int = Calendar.MONDAY
                profile.locations.find { location ->
                    location.day == day
                }?.let { location ->
                    updateLocation(location, imageView)
                }
            }
        }
        binding.profileUpdateLocationTueIc.setOnClickListener {
            binding.profileUpdateLocationTueIc.let { imageView ->
                val day: Int = Calendar.TUESDAY
                profile.locations.find { location ->
                    location.day == day
                }?.let { location ->
                    updateLocation(location, imageView)
                }
            }
        }
        binding.profileUpdateLocationWedIc.setOnClickListener {
            binding.profileUpdateLocationWedIc.let { imageView ->
                val day: Int = Calendar.WEDNESDAY
                profile.locations.find { location ->
                    location.day == day
                }?.let { location ->
                    updateLocation(location, imageView)
                }
            }
        }
        binding.profileUpdateLocationThuIc.setOnClickListener {
            binding.profileUpdateLocationThuIc.let { imageView ->
                val day: Int = Calendar.THURSDAY
                profile.locations.find { location ->
                    location.day == day
                }?.let { location ->
                    updateLocation(location, imageView)
                }
            }
        }
        binding.profileUpdateLocationFriIc.setOnClickListener {
            binding.profileUpdateLocationFriIc.let { imageView ->
                val day: Int = Calendar.FRIDAY
                profile.locations.find { location ->
                    location.day == day
                }?.let { location ->
                    updateLocation(location, imageView)
                }
            }
        }
        return binding.root
    }

    private fun setLocation(location: Location) = when (location.day) {
        Calendar.MONDAY -> binding.profileUpdateLocationMonIc
        Calendar.TUESDAY -> binding.profileUpdateLocationTueIc
        Calendar.WEDNESDAY -> binding.profileUpdateLocationWedIc
        Calendar.THURSDAY -> binding.profileUpdateLocationThuIc
        else -> binding.profileUpdateLocationFriIc
    }.let { updateImage(location, it) }

    private fun updateLocation(location: Location, imageView: ImageView) {
        location.value = when (location.value) {
            LocationEnum.WORK.name -> LocationEnum.REMOTE.name
            LocationEnum.REMOTE.name -> LocationEnum.VACATION.name
            LocationEnum.VACATION.name -> LocationEnum.UNKNOWN.name
            else -> LocationEnum.WORK.name
        }
        updateImage(location, imageView)
    }

    private fun updateImage(location: Location, imageView: ImageView) {
        when (location.value) {
            LocationEnum.WORK.name -> {
                imageView.setImageResource(R.drawable.ic_workcase)
                imageView.contentDescription = getString(R.string.ic_desc_disp_work)
            }
            LocationEnum.REMOTE.name -> {
                imageView.setImageResource(R.drawable.ic_home)
                imageView.contentDescription = getString(R.string.ic_desc_disp_home)
            }
            LocationEnum.VACATION.name -> {
                imageView.setImageResource(R.drawable.ic_sun)
                imageView.contentDescription = getString(R.string.ic_desc_disp_holiday)
            }
            else -> {
                imageView.setImageResource(R.drawable.ic_off)
                imageView.contentDescription = getString(R.string.ic_desc_disp_off)
            }
        }
    }
}