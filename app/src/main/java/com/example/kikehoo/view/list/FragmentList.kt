package com.example.kikehoo.view.list

import android.icu.util.Calendar
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kikehoo.R
import com.example.kikehoo.databinding.FragmentListBinding
import com.example.kikehoo.model.entity.Location
import com.example.kikehoo.model.entity.LocationEnum
import com.squareup.picasso.Picasso
import kotlinx.coroutines.InternalCoroutinesApi

class FragmentList : Fragment() {
    private lateinit var binding: FragmentListBinding
    @InternalCoroutinesApi
    private lateinit var viewModel:ListViewModel


    @OptIn(InternalCoroutinesApi::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel = ViewModelProvider(this)[ListViewModel::class.java]
        binding = FragmentListBinding.inflate(inflater, container,false)
        viewModel.profile.observe(viewLifecycleOwner, { profile ->
            binding.profileCardUsername.text = profile.name
            if (profile.image.isNotEmpty()) Picasso.get().load(profile.image).into(binding.profileCardPicture)
            profile.locations.forEach { location -> setLocation(location) }
        })

        val adapter = ListAdapter()

        val personListRecyclerview = binding.personListRecyclerview
        personListRecyclerview.adapter = adapter
        personListRecyclerview.layoutManager = LinearLayoutManager(requireContext())

        viewModel.personList.observe(
            viewLifecycleOwner,
            {person -> adapter.setData(person)}
        )
        binding.profileCardPicture.setOnClickListener{
            findNavController().navigate(R.id.action_fragmentList_to_fragmentProfile)
        }
        binding.profileCardUsername.setOnClickListener{
            findNavController().navigate(R.id.action_fragmentList_to_fragmentProfile)
        }
        binding.profileCardAvailabilityList.setOnClickListener{
            findNavController().navigate(R.id.action_fragmentList_to_fragmentProfile)
        }
        binding.profileCardActionAdd.setOnClickListener{
            findNavController().navigate(R.id.action_fragmentList_to_fragmentQrScann)
        }
        binding.profileCardActionSearch.setOnClickListener{
            binding.profileCardUsername.text = viewModel.personList.value?.find { person -> person.id == 1 }?.name
        }
        return binding.root
    }

    private fun setLocation(location: Location) = when (location.day) {
        Calendar.MONDAY -> binding.profileCardAvailabilityMon
        Calendar.TUESDAY -> binding.profileCardAvailabilityTue
        Calendar.WEDNESDAY -> binding.profileCardAvailabilityWed
        Calendar.THURSDAY -> binding.profileCardAvailabilityThu
        else -> binding.profileCardAvailabilityFri
    }.let {
        when (location.value) {
            LocationEnum.WORK.name -> {
                it.setImageResource(R.drawable.ic_workcase)
                it.contentDescription = getString(R.string.ic_desc_disp_work)
            }
            LocationEnum.REMOTE.name -> {
                it.setImageResource(R.drawable.ic_home)
                it.contentDescription = getString(R.string.ic_desc_disp_home)
            }
            LocationEnum.VACATION.name -> {
                it.setImageResource(R.drawable.ic_sun)
                it.contentDescription = getString(R.string.ic_desc_disp_holiday)
            }
            else -> {
                it.setImageResource(R.drawable.ic_off)
                it.contentDescription = getString(R.string.ic_desc_disp_off)
            }
        }
    }
}
