package com.example.kikehoo.view.qr

import android.annotation.SuppressLint
import android.content.ContentValues
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.coroutines.InternalCoroutinesApi
import net.glxn.qrgen.android.QRCode
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.kikehoo.databinding.FragmentQrgenBinding
import com.example.kikehoo.R


class FragmentQrGen : Fragment() {
    private val noCompression = 100
    private val qrCodeFileName = "myqrcode"
    private lateinit var binding: FragmentQrgenBinding

    @InternalCoroutinesApi
    private lateinit var qrGenViewModel: QrGenViewModel

    @SuppressLint("WrongThread")
    @OptIn(InternalCoroutinesApi::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentQrgenBinding.inflate(inflater, container, false)

        qrGenViewModel = ViewModelProvider(this)[QrGenViewModel::class.java]

        qrGenViewModel.profile.observe(viewLifecycleOwner, { agenda ->

            val json = qrGenViewModel.getJson(agenda)
            val bitmap: Bitmap = QRCode.from(json).bitmap()
            binding.qrCodeView.setImageBitmap(bitmap)

            try {
                val values = ContentValues()
                values.put(MediaStore.MediaColumns.DISPLAY_NAME, qrCodeFileName)
                values.put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
                values.put(
                    MediaStore.MediaColumns.RELATIVE_PATH,
                    Environment.DIRECTORY_DOCUMENTS.toString() + "/Kikeou/"
                )
                val resolver = activity!!.contentResolver
                val uri = resolver.insert(MediaStore.Files.getContentUri("external"), values)
                val out = uri?.let { resolver.openOutputStream(it) }
                bitmap.compress(Bitmap.CompressFormat.PNG, noCompression, out)
                out?.flush()
                out?.close()

                binding.qrGenActionBack.setOnClickListener {
                    findNavController().navigate(R.id.action_fragmentQRGen_to_fragmentProfile)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
        return binding.root
    }
}